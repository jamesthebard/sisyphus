message = dict()
'''Used to pass worker status message to the heartbeat thread.'''

is_connected_to_redis = True
'''To keep the worker online during Redis connectivity issues'''
